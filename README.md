# Beersmith Arch Package
Create Arch package from Beersmith 2.3 deb

## Prerequisites
Install **libpng12**, **webkitgtk2-bin** and **icu71-bin** from AUR

## Installation (Arch)
* Clone this repo (git clone https://gitlab.com/siberite/beersmith2-archpackage.git)
* cd beersmith2-archpackage
* makepkg -sri

I suggest you also install **makedep** from the AUR to stop icu71 files from being recognised as orphans
This can be prevented by running:
* sudo makedep beersmith2-robvdw -a icu71-bin
* pacman -Qi beersmith2-robvdw icu71-bin
just to check dependencies are amended afterwards

## Source package
http://beersmith.com/download-beersmith/

## Credits
Based on AUR package beersmith2 by Greg Greenaae <ggreenaae@gmail.com>
Based on AUR package beersmith-robvdw by Rob Vandeweyer <rob@robvdw.net>

